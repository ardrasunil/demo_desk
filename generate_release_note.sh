#!/bin/bash

#variables for api call
GITLAB_API_URL="https://gitlab.com/api/v4"
ACCESS_TOKEN="glpat-5QQHBNppoxXknAGKkBfg"
PROJECT_ID="$CI_PROJECT_ID"
SOURCE_BRANCH="$CI_COMMIT_REF_NAME"

#print the variables to verify
echo "ACCESS TOKEN : $ACCESS_TOKEN"
echo "GITLAB API URL :$GITLAB_API_URL"
echo "PROJECT ID : $PROJECT_ID"
echo "SOURCE BRANCH :"

#api call to get the mr id
MR_ID=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" $GITLAB_API_URL/projects/$PROJECT_ID/merge_requests?source_branch=$SOURCE_BRANCH | jq -r '.[0].iid')
echo "$MR_ID"

#api call to get the commits of mr 
MR_COMMIT=$(curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_API_URL/projects/$CI_PROJECT_ID/merge_requests/$MR_ID/commits" | jq -r '.[] | "\(.title) by \(.committer_name) on \(.committed_date)"')
echo "COMMITS
"
echo "$MR_COMMIT"
echo "$MR_COMMIT" > mr_commit.txt

TARGET_BRANCH="release"
git checkout "$TARGET_BRANCH"
git add mr_commit.txt
git commit -m "Adding release note to release branch"
git push origin "$TARGET_BRANCH"


